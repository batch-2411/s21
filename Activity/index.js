// 1
/*
Create a function which is able to receive a single argument and add the input at the end of the users array.
    Function should be able to receive a single argument.
    Add the input data at the end of the array.
    The function should not be able to return data.
    Invoke and add an argument to be passed in the function.
    Log the users array in the console.
*/
let usersArray = ['Dwayne Johnson', 'Steve Austin', 'Kurt Angle', 'Dave Bautista'];
console.log("Original Array:");
console.log(usersArray);
function addElement(element) {
	usersArray[usersArray.length] = element;
}
addElement('John Cena');
console.log(usersArray);

// 2
/*
Create a function which is able to receive an index number as a single argument return the item accessed by its index.
    function should be able to receive a single argument.
    return the item accessed by the index.
    Create a global variable called outside of the function called itemFound and store the value returned by the function in it.
    log the itemFound variable in the console.
*/
function accessElement(index) {
	return usersArray[index];
}

let itemFound = accessElement(2);
console.log(itemFound);

// 3
/*
Create a function which is able to delete the last item in the array and return the deleted item.
    Create a function scoped variable to store the last item in the users array.
    Shorten the length of the array by at least 1 to delete the last item.
    Return the last item in the array which was stored in the function scoped variable.
    Create a global scoped variable outside of the function and store the result of the function.
    Log the global scoped variable in the console.
*/
function deleteLastElement() {
	let deletedElement = usersArray[usersArray.length - 1];
	usersArray.length = usersArray.length - 1;
	return deletedElement;
}

let itemDeleted = deleteLastElement();
console.log(itemDeleted);
console.log(usersArray);
// 4
/*
Create a function which is able to update a specific item in the array by its index.
    Function should be able to receive 2 arguments, the update and the index number.
    First, access and locate the item by its index then re-assign the item with the update.
    This function should not have a return.
    Invoke the function and add the update and index number as arguments.
    Outside of the function, Log the users array in the console.
*/
function updateElement(updateElement, index) {
	usersArray[index] = updateElement;
}
updateElement('Triple H', 3);
console.log(usersArray);

// 5
/*
Create a function which is able to delete all items in the array.
    You can modify/set the length of the array.
    The function should not return anything.
    Invoke the function.
    Outside of the function, Log the users array in the console.
*/
function clearArray() {
	usersArray.length = 0;
}
clearArray();
console.log(usersArray);

// 6
/*
Create a function which is able to check if the array is empty.
    Add an if statement to check if the length of the users array is greater than 0.
    If it is, return false.
    Else, return true.
    Create a global variable called outside of the function called isUsersEmpty and store the returned value from the function.
    Log the isUsersEmpty variable in the console.
*/
function checkEmptyArray() {
	if (usersArray.length > 0) {
		return false;
	} else {
		return true;
	};
};

let isUsersEmpty = checkEmptyArray();
console.log(isUsersEmpty);